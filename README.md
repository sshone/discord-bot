A bot for playing back music on a Discord voice channel.

Features:

* Ability to play music from a streaming source such as YouTube, given a URL
* Commands to summon bot to voice channel
* Ability to queue up songs
* Playback commands such as play, stop, pause, resume
* Vote skip functionality
* Volume control functionality