import discord
import asyncio
from discord.ext import commands

if not discord.opus.is_loaded():
	discord.opus.load_opus('opus')

bot_token = '' # The token provided by the Discord API for the bot to log in with
command_prefix = '?' # The prefix the bot uses to identify a command
description = 'A simple bot created using the python.py library, intended for simple music playing on one Discord server.'

class SongEntry:
	def __init__(self, message, player):
		print('initialised')
		self.requester = message.author
		self.channel = message.channel
		self.player = player

	def __str__(self):
		fmt = '*{0.title}* uploaded by {0.uploader} and requested by {1.display_name}'
		duration = self.player.duration
		if duration:
			fmt = fmt + ' [length: {0[0]}m {0[1]}s]'.format(divmod(duration, 60))
		return fmt.format(self.player, self.requester)

class MusicBot:
	def __init__(self, bot):
		self.bot = bot
		self.current_song = None
		self.voice = None
		self.play_next_song = asyncio.Event()
		self.songs = asyncio.Queue()
		self.skip_votes = set()
		self.audio_player = self.bot.loop.create_task(self.audio_player_task())
		self.last_bot_message = None

	def is_playing(self):
		if self.voice is None and self.current_song is None:
			return False
		return not self.current_song.player.is_done()

	def get_next(self):
		# Send "Play Next Song" event
		self.bot.loop.call_soon_threadsafe(self.play_next_song.set)

	def skip_song(self):
		self.skip_votes.clear()
		if self.is_playing():
			self.current_song.player.stop()

	async def audio_player_task(self):
		while True:
			self.play_next_song.clear()
			self.current_song = await self.songs.get()
			await self.bot.send_message(self.current_song.channel, '```Now playing: ' + str(self.current_song.player.title) + '```')
			self.current_song.player.start()
			await self.play_next_song.wait()

			# Clear previous votes to skip
			self.skip_votes.clear()

			# If there are no more songs left to play disconnect from the voice channel
			if self.songs.empty():
				await self.voice.disconnect()

	@commands.command()
	# Adds two numbers together
	async def add(self, left : int, right : int):
		await bot.say(left + right)

	@commands.command(pass_context=True, no_pm=True)
	# Joins the user's audio channel and plays a given song
	async def play(self, context, *, song : str):
		# Delete the user's message to help prevent clutter in chat
		await self.bot.delete_message(context.message);

		summoned_channel = context.message.author.voice_channel
		if summoned_channel is None:
			await bot.say('You must be in a voice channel to use this command.')
			return False

		# If bot not already connected to a voice channel
		if bot.is_voice_connected(context.message.server) is False:
			self.voice = await bot.join_voice_channel(summoned_channel)
		#else:
			#await bot.move_to(summoned_channel)


		if song is None:
			await bot.say('Error: You must provide a song name or URL.')
			return False

		ytdl_opts = {
			'default_search':'ytsearch',
		}

		# Create the youtube video player to play the song
		try:
			player = await self.voice.create_ytdl_player(song, ytdl_options=ytdl_opts, after=self.get_next)
		except Exception as e:
			fmt = 'An error occurred while processing this request: ```py\n{}: {}\n```'
			await bot.send_message(context.message.channel, fmt.format(type(e).__name__, e))
		else:
			player.volume = 1
			entry = SongEntry(context.message, player)
			await self.songs.put(entry)
			await bot.say("Added: " + player.title + " to queue.")

		return True

	@commands.command(pass_context=True, no_pm=True)
	# Stops playing audio and leaves the voice channel, as well as clearing the current song queue
	async def stop(self, ctx):
		# Delete the user's message to help prevent clutter in chat
		await self.bot.delete_message(ctx.message);

		if self.is_playing():
			self.current_song.player.stop()
			try:
				await self.voice.disconnect()
			except:
				pass
		else:
			await bot.say('Nothing is playing...')

	@commands.command(pass_context=True, no_pm=True)
	# Pause the currently playing song
	async def pause(self, ctx):
		# Delete the user's message to help prevent clutter in chat
		await self.bot.delete_message(ctx.message);

		if self.is_playing():
			self.current_song.player.pause()
			await self.bot.say('Song paused, type ?resume to resume.')

	@commands.command(pass_context=True, no_pm=True)
	# Resume a paused song
	async def resume(self, ctx):
		# Delete the user's message to help prevent clutter in chat
		await self.bot.delete_message(ctx.message);

		if self.is_playing():
			self.current_song.player.resume()
			await self.bot.say('Resuming song.')

	@commands.command(pass_context=True, no_pm=True)
	# Displays information about the currently playing song
	async def song(self, ctx):
		# Delete the user's message to help prevent clutter in chat
		await self.bot.delete_message(ctx.message);

		if self.is_playing():
			votes_to_skip = (len(self.voice.channel.voice_members) - 1) / 2 # Number of users in the voice channel minus the bot divided by 2
			await self.bot.say('Now playing {} [Skips: {}/{}]'.format(self.current_song.player.title, 0, votes_to_skip))
		else:
			await self.bot.say('No song is playing.')

	@commands.command(pass_context=True, no_pm=True)
	async def skip(self, ctx):
		# Delete the user's message to help prevent clutter in chat
		await self.bot.delete_message(ctx.message);

		votes_to_skip = int ((len(self.voice.channel.voice_members) - 1) / 2) # Number of users in the voice channel minus the bot divided by 2
		if not self.is_playing():
			await self.bot.say('No song is playing.')
			return

		voter = ctx.message.author
		if voter == self.current_song.requester:
			await self.bot.say('Skipping song...')
			self.skip_song()
		elif voter.id not in self.skip_votes:
			self.skip_votes.add(voter.id)
			if len(skip_votes) >= votes_to_skip:
				await self.bot.say('Vote passed, skipping song...')
				self.skip_song()
			else:
				await self.bot.say('Skips [{}/{}]'.format(total_votes, votes_to_skip))
		else:
			await self.bot.say('You have already voted previously.')

bot = commands.Bot(command_prefix='?', description=description)
bot.add_cog(MusicBot(bot))

@bot.event
async def on_ready():
	print('Successful login as')
	print(bot.user.name)
	print(bot.user.id)
	print('-------------------')

# Log in as the bot using its token
bot.run(bot_token)